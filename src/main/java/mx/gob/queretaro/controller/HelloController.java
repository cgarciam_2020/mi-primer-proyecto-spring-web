package mx.gob.queretaro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
//La clase debe tener un request mapping (path)
@RequestMapping("/hello")
public class HelloController {
	
	//Aqui cada metodo que se haga, se convierte en una pagina nueva.
	
	//Primer ruta mediante un metodo
	@RequestMapping(method = RequestMethod.GET)
	public String printHello(ModelMap model) {
		model.addAttribute("message", "Hello Spring MVC Framework!");// Nombre de parámetro y valor

		return "hello";
	}

}
