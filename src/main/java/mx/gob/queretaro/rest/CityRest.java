package mx.gob.queretaro.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("ciudad")


public class CityRest {
    /*
    @GetMapping("saludo")
    public String hello (){
        return "Hola Mundo desde un rest";
    }*/

    //Ejemplo devolviendo un json (se puede consmir también pro aplicacions moviles)
    @GetMapping(path = "saludo", produces = MediaType.APPLICATION_JSON_VALUE)
    public String hello (){
        return "Hola Mundo desde un rest, devolviendo un json";
    }

    @GetMapping("saludo 2")
    public String hello2 () {
        return "Hola";
    }
    
}